# MODCLOTH RECOMMENDATION
## Description
Product size recommendation and fit prediction are critical in order to improve customers’ shopping experiences and to reduce product return rates. However, modeling customers’ fit feedback is challenging due to its subtle semantics, arising from the subjective evaluation of products and imbalanced label distribution (most of the feedbacks are "Fit"). These datasets, which are the only fit related datasets available publically at this time, collected from ModCloth and RentTheRunWay could be used to address these challenges to improve the recommendation process.

Following type of information is available in the datasets:                                                                                          
ratings and reviews                                                                                                                                  
fit feedback (small/fit/large)                                                                                                                       
customer/product measurements                                                                                                                        
category information

Note that, here a ‘product’ refers to a specific size of a product, as our goal is to predict fitness for associated catalog sizes. Also, since different clothing products use different sizing conventions, we standardize sizes into a single numerical scale preserving the order.
## Team Members

1. U.Shivani       : 19WH1A1279 : IT
2. U.Saisree       : 19WH1A1269 : IT
3. R.Sreeja        : 19WH1A0473 : ECE
4. T.Nagajyothi    : 19WH1A0586 : CSE
5. K.Misba Fathima : 19WH1A05A1 : CSE
